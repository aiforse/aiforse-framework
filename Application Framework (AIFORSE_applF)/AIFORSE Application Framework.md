# Application Framework (AIFORSE_applF)

The Application Framework (AIFORSE_applF) is a sub-component of [AIFORSE_xF](./README.md), the AIFORSE Community’s blueprint for enabling and utilizing artificial intelligence for effective and efficient software engineering.

It provides a common language and means of identification for buyers and suppliers across all software tools, used for software engineering.

All of AFORSE_xF, including the Process Framework, is created and evolved by industry leaders and practitioners in AIFORSE Community’s member-driven [collaboration community](https://www.aiforse.org/collaboration).



## What is the Application Framework?

The Application Framework (AIFORSE_applF) provides a systems map which captures how software processes are implemented in deployable, recognizable applications.

The Application Framework provides a common language for communities who specify, procure, design, and sell software engineering systems, so that they can understand each other’s viewpoints. It provides logical groupings of applications, then describes each application’s functionality.

As a result, it is a practical, everyday working guide to define and navigate the elements of the complex management systems landscape.



## 6 things you can do with the Application Framework

1. Explore, study and employ artificial intelligence driven tools to increase efficiency of software engineering operations.
2. Streamline procurement by using common definitions and language to specify and evaluate software engineering solutions.
3. Document and then rationalize your application inventory when launching a new software company,  transforming existing infrastructure or dealing with mergers and acquisitions.
4. Integrate faster and with lower costs by defining and clearly communicating the functions provided within each application.
5. Reduce custom development costs with modular, standard application requirements.
6. Increase automation and efficiency with standard, deployable components.