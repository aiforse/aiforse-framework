# Application Framework Principles



The list of principles which drive AIFORSE Application Framework approaches to software engineering applications ("APPLICATION") development and usage is provided below.



## Principles

APPLICATION must be:

* **[DEFINED-BY-PROCESS](#defined-by-process)**
* **[DATA-FLOW-ORIENTED](#data-flow-oriented)**



## Explanations and Examples

### **DEFINED-BY-PROCESS**

An APPLICATION shall provide a Solution for a particular PROCESS (or/and their parts), defined in the [Process Framework](../Process%20Framework (AIFORSE_procF)/), and hence, shall realize all the [Process Framework Principles](../Process%20Framework (AIFORSE_procF)/Process%20Framework%20Principles.md).



### **DATA-FLOW-ORIENTED**

An APPLICATION shall identify a complete set of ARTIFACTs - which are used (read) as an *input* to an APPLICATION and which are affected (created/updated/deleted) as an *output* of an APPLICATION - defined in the [Information Framework](../Information%20Framework (AIFORSE_infoF)/), and hence, shall realize all the [Information Framework Principles](../Information%20Framework (AIFORSE_infoF)/Information%20Framework%20Principles.md).