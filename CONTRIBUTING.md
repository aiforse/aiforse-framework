# Contribution Guide

[AIFORSE Framework](./README.md) project accepts contributions via git merge requests.
This document outlines some of the conventions on submission workflow, commit message formatting, contact points, and other resources to make it easier to get your contribution done and accepted.



## Access

Contribution commits are allowed only for authorized users who are granted a status of "Contribution Member". If you don't have these privileges, please request them by writing an email to [info@aiforse.org](mailto:info@aiforse.org).



## How to Contribute

Please, follow the next procedure:

1. Create a new branch from the master;
   * It can be named after you or a "Feature" which you would like to add.
2. Check-out new branch locally.
3. Make changes to existing files / add new files.
4. Commit changes;
   * The commit summary must start with "Added"/"Updated"/"Removed" and end with a dot,
   * Each commit shall be signed - see "[Contributor License Agreement](#contributor-license-agreement)" below for more details.
5. Push changes to your branch.
6. Create new [Merge Request](https://gitlab.com/aiforse/aiforse-framework/merge_requests) from your branch to the master.
7. Wait for a Merge Request passes the personal evaluation of the Project Administrator.



## Support Channels

The official support channels, for both users and contributors, are:

- GitLab [Issues](https://gitlab.com/aiforse/aiforse-framework/issues)*

*Before opening a new issue or submitting a new pull request, it's helpful to
search the project - it's likely that another user has already reported the
issue you're facing, or it's a known issue that we're already aware of.



## Contributor License Agreement

By contributing to AIFORSE Community you agree to the [AIFORSE Community Individual Contributor License Agreement (ACICLA)](AIFORSE%20Community%20Individual%20Contributor%20License%20Agreement.md). 

In order to show your agreement with the ACICLA, you should include at the end of a commit message
the following line: `Signed-off-by: John Doe <john.doe@example.com>`, using your real name.

This can be done using the [`-s`](https://git-scm.com/docs/git-commit) flag on the `git commit`.



## Recommended Tools

* **[SourceTree](https://www.sourcetreeapp.com/)** - a free Git client for Windows and Mac (during the installation the system prompts to enter Atlassian/GitHub credentials - this step can be skipped). When committing, there're following commit options to be used:
  * "Sign off"
  * "Create pull request"
* **[Typora](https://typora.io/)** - a free markdown editor/reader for Windows and Mac.