# Information Framework Principles

The list of principles which drive AIFORSE Information Framework approaches to software engineering artifacts ("ARTIFACT") management is provided below.



## Principles

ARTIFACT must be:

- **[TOOL-AGNOSTIC](#tool-agnostic)**
- **[IDENTIFIABLE](#identifiable)**
- **[COMPARABLE](#comparable)**
- **[VERSIONABLE](#versionable)**
- **[SEARCHABLE](#searchable)**
- **[ACCESSIBLE](#accessible)**



## Explanations and Examples

### **TOOL-AGNOSTIC**

All the forthcoming principles must be realized on the ARTIFACT Source Data Level, but not on the level of an Application, which manages this ARTIFACT.

* **DOs**: 
  * Source Code - in general, can be stored in any VCS (version control system) and can be managed by any IDE (integrated development environment) or even without it.
* **DON'Ts**: 
  * Technical Design, stored in MS Word format, is finally stored as a binary file, which cannot be opened without MS Word application.



### **IDENTIFIABLE** 

Each ARTIFACT (and event its specific parts) shall be treated as Asset, which can be tracked, referenced and located by its unique identifier (not necessarily globally unique). 

And such identifier shall be available not on the Application level, but on the Data level, to enforce the TOOL-AGNOSTIC Principle.

* **DOs:**
  * JIRA Ticket - has a human-readable unique identifier within its Project.
  * JAVA Method - has a unique name within its Interface/Class. 
* **DON'Ts:**
  * User Story, captured in a spreadsheet as just its name without any artificial identifier.



### **COMPARABLE** 

It shall be possible to compare two different ARTIFACTs in order to identify how each of them differs from another one and what the common part is.

And such comparison shall be able to be done not only on the Application level, but also on the Data level, to enforce the TOOL-AGNOSTIC Principle.

* **DOs:**
  * Test Cases, written in Gherkin language, compared by any Diff Tool.
* **DON'Ts:**
  * Software Diagrams (e.g. Activity Flow Diagram) stored as MS Visio Files.
  * Configuration File, stored as Excel Spreadsheet.



### **VERSIONABLE** 

It shall be possible to version an ARTIFACTs in order to identify *which changes* are done, *when* and *by whom*. 

And such versioning shall be able to be done not only on the Application level, but also on the Data level, to enforce the TOOL-AGNOSTIC Principle.

- **DOs:**
  - Configuration File, stored in the YAML Format, versioned in any VCS.
- **DON'Ts:**
  - User Interface Prototypes, created in Axure RP or Marvel App.



### **SEARCHABLE**

It shall be possible to text-search within a content of an ARTIFACTs in order to find needed information or locate some of its parts. 

And such search shall be able to be done not only on the Application level, but also on the Data level, to enforce the TOOL-AGNOSTIC Principle.

- **DOs:**
  - Configuration File, stored in the YAML Format, versioned in any VCS.
- **DON'Ts:**
  - Software Diagrams (e.g. Activity Flow Diagram) stored as pictures.



### **ACCESSIBLE**

It shall be possible to save, store and retrieve an ARTIFACT through a commonly available interface in order to find needed information.

And such access shall be able to be done not only on the Application level, but also on the Data level, to enforce the TOOL-AGNOSTIC Principle.

- **DOs:**
  - Centralized Document Server with configured Authentication and Authorization Privileges.
- **DON'Ts:**
  - ARTIFACTS stores as pictures and attachments in mailboxes.