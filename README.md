# AIFORSE Framework (AIFORSE_xF)

AIFORSE Framework (AIFORSE_xF) is a suite of best practices and standards that enables and utilizes artificial intelligence for effective and efficient software engineering. 

It enables you to assess and optimize performance using a proven, data-oriented approach to operations and integration. The practical tools available in AIFORSE_xF help improve end-to-end management of services across complex environments.

AIFORSE_xF is aimed to improve agility in product development, solution delivery and outsourcing Processes, resulting in increased margins, lower costs, and optimal customer experience. AIFORSE_xF is created and evolved by AIFORSE Community members.

AIFORSE_xF also includes adoption best practices to help companies implement and use the standards and management best practices to ensure ongoing conformance.



## AIFORSE Framework Structure

AIFORSE_xF includes four Frameworks representing different viewpoints on software engineering, which used in conjunction provide a comprehensive approach to enable and apply artificial intelligence for software engineering: 

* **[Process Framework (AIFORSE_procF)](Process%20Framework%20(AIFORSE_procF)/AIFORSE%20Process%20Framework.md)**
* **[Information Framework (AIFORSE_infoF)](Information%20Framework%20(AIFORSE_infoF)/AIFORSE%20Information%20Framework.md)**
* **[Application Framework (AIFORSE_applF)](Application%20Framework%20(AIFORSE_applF)/AIFORSE%20Application%20Framework.md)**
* **[Integration Framework (AIFORSE_intgF)](Integration%20Framework%20(AIFORSE_intgF)/AIFORSE%20Integration%20Framework.md)**



## Who AIFORSE Framework is intended for

Mainly, for

* Software engineering companies (product development, solution delivery, outsourcing)
* Companies, which develop tools (both AI- and non-AI-driven) for software engineering companies
* Companies, which use custom software solutions, critical for their business and/or operations

Also, recommended for

* Companies, which develop in-house software for their own purposes
* Researchers, consultants, investors, working in software engineering related fields



## What AIFORSE Framework can do for You 

- Enable data-driven processes and artifacts to apply artificial intelligence methods to them
- Reduce costs, minimize time-to-market and innovate with streamlined end-to-end process management
- Create, deliver and manage enterprise-grade software
- Improve customer experience and retention using proven processes, metrics and maturity models
- Increase traceability of software development phases
- Increase quality and maintainability of software
- Optimize business processes to deliver highly efficient, automated operations
- Reduce integration costs and risk through standardized interfaces and a common information model
- Reduce fast growth risk by utilizing a blueprint for agile, efficient operations
- Improve efficiency of employees by following a uniform methodology
- Get insights and recommendations about applying AI to software engineering processes
- Verify and benchmark your AI models, applied to software engineering processes
- Gain independence and confidence in your procurement choices through conformance certification and procurement guides
- Gain clarity by providing a common, industry-standard language



## Contribution Guide

Read the [Contribution Guide](CONTRIBUTING.md).