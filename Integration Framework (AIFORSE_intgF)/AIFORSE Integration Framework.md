# Integration Framework (AIFORSE_intgF)

The Integration Framework (AIFORSE_intgF) is a component of [AIFORSE_xF](./README.md), the AIFORSE Community’s blueprint for enabling and utilizing artificial intelligence for effective and efficient software engineering. 

It provides a complete set of standards to integrate diverse software engineering applications.

All of AFORSE_xF, including the Process Framework, is created and evolved by industry leaders and practitioners in AIFORSE Community’s member-driven [collaboration community](https://www.aiforse.org/collaboration).



## What is the Integration Framework?

The Integration Framework (AIFORSE_intgF) contains a set of standards that support the integration and interoperability between applications defined in the Applications Framework (AIFORSE_applF). These standards have been widely adopted and are in use in the software engineering industry today.

The Integration Framework, used in conjunction with the other AIFORSE Frameworks and reference architectures, allows software engineering companies to identify the key integration points in their architectures, and define them in terms of widely adopted industry standard APIs.



## 4 things you can do with the Integration Framework

1. Easily plug-in and test new tools, including artificial intelligence driven, into existing applications infrastructure.
2. Complete consistent set of standards for both inter and intra software engineering company integration available off the shelf to use immediately removes investment needed to create proprietary standards.
3. Ever increasing number of new software engineering applications allows rapid integration with new tools and partners.
4. Wide adoptions across software engineering applications vendors enable rapid integration of multi vendor solutions and prevent vendor lock-in.