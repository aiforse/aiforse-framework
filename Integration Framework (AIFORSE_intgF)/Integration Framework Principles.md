# Integration Framework Principles

The list of principles which define AIFORSE Integration Framework is provided below.



## Principles

The Principles are:

- [OPEN API](#open-api) [^1]
- [OPEN ESB](#open-esb) [^2]



## Explanations and Examples

### **OPEN API**

Each of the Application, defined in the Application Framework, must provide Public API, which can be used to integrate this Application into the End-to-End Software Engineering Process.

* **DOs**: 
  * Git Command Line Instructions - can be easily managed with Automated Scrips.
  * JIRA REST API - can be used to automatically report a new Ticket, resolve existing one etc.
* **DON'Ts**: 
  * Invision - it doesn't provide API, which makes the Programmatic Access to Design Information captured in Invision files impossible.



### **OPEN ESB**

Each of the Application, defined in the Application Framework, should avoid Point-to-Point Integration with all other Applications, defined in the Application Framework, but instead provide an Adaptor to a single Bus, which shall be owned and maintained by a community (free and open-source).

- **DOs**: 
  - Elastic Stack & Apache Kafka - provides a configurable toolbox, which can be embedded into existing architecture, independent on already existing parts of it, to log software-related operations.
- **DON'Ts**: 
  - Slack - it provides integration adapters to different tools (e.g. JIRA, Asana, Jenkins CI etc.), but this approach couples systems together tightly and creates excessive dependencies.



------


[^1]: Not to be confused with [OpenAPI](https://www.openapis.org).
[^2]: Not to be confused with [OpenESB](http://www.open-esb.net).