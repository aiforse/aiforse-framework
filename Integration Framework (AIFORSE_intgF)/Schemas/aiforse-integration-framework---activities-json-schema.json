{
    "$schema": "http://json-schema.org/draft-2019-09/schema#",
    "$id": "http://aiforse.org/framework/integration/schemas/activities.json",
    "title": "AIFORSE Integration Framework - Activities JSON Schema",
    "type": "object",
    "properties": {
        "links": {
            "type": "object",
            "properties": {
                "self": {
                    "type": "string"
                }
            },
            "required": [
                "self"
            ]
        },
        "data": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/activities"
            },
            "default": []
        }
    },
    "required": [
        "data"
    ],
    "definitions": {
        "activities": {
            "Name": "Activities",
            "Description": "Any Action/ Interaction/ Task/ Communication/ Step/ etc., which occurs in a Process",
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "const": "activities"
                },
                "id": {
                    "type": "string",
                    "pattern": "^([-0-9A-Za-z_]+)$"
                },
                "attributes": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        },
                        "flow-type": {
                            "type": "string",
                            "enum": [
                                "SYNC",
                                "ASYNC"
                            ]
                        }
                    }
                },
                "relationships": {
                    "type": "object",
                    "properties": {
                        "actor-a": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "http://aiforse.org/framework/integration/schemas/actors.json#/definitions/actors"
                                    },
                                    "default": {}
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        },
                        "actor-b": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "http://aiforse.org/framework/integration/schemas/actors.json#/definitions/actors"
                                    },
                                    "default": {}
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        },
                        "input": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/definitions/resource-objects"
                                    },
                                    "default": []
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        },
                        "output": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/definitions/resource-objects"
                                    },
                                    "default": []
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        },
                        "depends-on": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/definitions/activities"
                                    },
                                    "default": []
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        }
                    },
                    "required": [
                        "actor-a"
                    ]
                }
            },
            "required": [
                "type",
                "id"
            ]
        },
        "resource-objects": {
            "Name": "Resource Objects",
            "Description": "Representation of a Resource Instances, with their States and Parameters Values",
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "const": "resource-objects"
                },
                "id": {
                    "type": "string",
                    "pattern": "^([-0-9A-Za-z_]+)$"
                },
                "relationships": {
                    "type": "object",
                    "properties": {
                        "resource-type": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "http://aiforse.org/framework/integration/schemas/resources.json#/definitions/resources"
                                    },
                                    "default": {}
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        },
                        "resource-state": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "http://aiforse.org/framework/integration/schemas/resources.json#/definitions/resource-states"
                                    },
                                    "default": {}
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        },
                        "parameters-values": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/definitions/resource-parameters-values"
                                    },
                                    "default": []
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        }
                    },
                    "required": [
                        "resource-type"
                    ]
                }
            },
            "required": [
                "type"
            ]
        },
        "resource-parameters-values": {
            "Name": "Resource Parameters Values",
            "Description": "Resource Instance Parameters Values",
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "const": "resource-parameters-values"
                },
                "id": {
                    "type": "string",
                    "pattern": "^([-0-9A-Za-z_]+)$"
                },
                "attributes": {
                    "type": "object",
                    "properties": {
                        "value": {
                            "type": "string"
                        }
                    }
                },
                "relationships": {
                    "type": "object",
                    "properties": {
                        "resource-parameters": {
                            "type": "object",
                            "properties": {
                                "links": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "#/definitions/relationships-links"
                                    }
                                },
                                "data": {
                                    "type": "object",
                                    "items": {
                                        "$ref": "http://aiforse.org/framework/integration/schemas/resources.json#/definitions/resource-parameters"
                                    },
                                    "default": {}
                                }
                            },
                            "required": [
                                "links",
                                "data"
                            ]
                        }
                    },
                    "required": [
                        "resource-parameters"
                    ]
                }
            },
            "required": [
                "type",
                "id"
            ]
        },
        "relationships-links": {
            "Name": "",
            "Description": "",
            "type": "object",
            "properties": {
                "self": {
                    "type": "string"
                },
                "related": {
                    "type": "string"
                }
            },
            "required": [
                "self",
                "related"
            ]
        }
    }
}