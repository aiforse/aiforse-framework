# Process Framework (AIFORSE_procF)

The Process Framework (AIFORSE_procF) is a critical component of [AIFORSE_xF](./README.md), the AIFORSE Community’s blueprint for enabling and utilizing artificial intelligence for effective and efficient software engineering. 

It is a multi-layered comprehensive view of the key processes required to manage and execute data-driven software engineering processes.

All of AFORSE_xF, including the Process Framework, is created and evolved by industry leaders and practitioners in AIFORSE Community’s member-driven [collaboration community](https://www.aiforse.org/collaboration).



## What is the Process Framework?

It is a hierarchical catalog of the key software engineering processes. At the conceptual level, the Framework has three major areas, reflecting major focuses within typical software development companies:

- Program and Project Management
- Software Development Operations
- Data Migration



## 7 things you can do with the Process Framework

1. Build logged, traceable, analyzable, reusable and data-driven end-to-end software engineering processes, ready to be partially or completely executed by artificial intelligence.
2. Create a common language for use across departments, systems, customers, external partners and suppliers, reducing cost and risk of system implementation, integration and delivery.
3. Adopt a standard structure, terminology and classification scheme for software engineering processes to simplify internal operations and maximize opportunities to partner.
4. Apply disciplined and consistent process development company-wide, allowing for cross-organizational/product/project reuse.
5. Understand, design, develop and manage software applications in terms of business process requirements so applications will better meet business needs.
6. Create consistent and high-quality end-to-end process flows, eliminating gaps and duplications in process flows.
7. Identify opportunities for cost and performance improvement through re-use of existing processes and systems.