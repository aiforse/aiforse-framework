# Process Framework Principles

> *If you can't describe what you are doing as a process, you don't know what you are doing.*
>
> -W. Edwards Deming



The list of principles which drive AIFORSE Process Framework approaches to software engineering processes ("PROCESS") management is provided below.



## Principles

PROCESS must be:

* **[DOCUMENTED](#documented)**
* **[DATA-FLOW-RELATED](#data-flow-related)**
* **[SEQUENTIAL](#sequential)**
* **[ASSIGNABLE](#assignable)**
* **[VERIFIABLE](#verifiable)**
* **[MEASURABLE](#measurable)**
* **[TRACEABLE](#traceable)**
* **[SCALABLE](#scalable)**



## Explanations and Examples

### **DOCUMENTED**

A PROCESS shall be documented in order to enable its execution analysis and all the forthcoming principles.

To make this information usable, a PROCESS shall be documented following the [Information Framework Principles](../Information%20Framework (AIFORSE_infoF)/Information%20Framework%20Principles.md).



### **DATA-FLOW-RELATED**

A PROCESS shall consist of atomic PARTs (steps), each of which shall have:

- identified a complete set of ARTIFACTs - which are used (read) as an *input* to this PART and which are affected (created/updated/deleted) as an *output* of this PART - defined in the [Information Framework](../Information%20Framework (AIFORSE_infoF)/).



### **SEQUENTIAL**

A PROCESS shall consist of atomic PARTs (steps), each of which shall have:

* identified DEPENDENCIES on other PROCESSes and/or PARTs.



### **ASSIGNABLE**

A PROCESS shall consist of atomic PARTs (steps), each of which shall have:

- identified ACTOR (individual/system), responsible for this PART.



### **VERIFIABLE**

A PROCESS shall consist of atomic PARTs (steps), each of which shall have:

- DEFINITION OF DONE



### **MEASURABLE**

A PROCESS shall consist of atomic PARTs (steps), each of which shall have:

- quantifiable METRICs which describe this PART.



### **TRACEABLE**

It shall be possible at any moment of the time to identify on which PART(-s) a particular PROCESS instance is.



### **SCALABLE**

A PROCESS and each of its atomic PARTs (steps) shall be able to be scaled.